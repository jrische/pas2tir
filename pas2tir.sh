#!/bin/sh
# Author:   Julien Rische <julien.rische@laposte.net>
# Created:  2020-04-22
# Modified: 2021-12-27
# Version:

LOG_LEVEL="${LOG_LEVEL:-4}"

prog="$(basename "$0")"

#### COMMON ####

getv() {
    eval 'echo -n "$'"$1"'"'
}

setv() {
    eval "${1}='$(echo -n "$2" | sed "s/'/'\\\\''/g")'"
}

push() {
    if [ $# -ge 2 ]; then
        local var="$1"
        shift
        local val="$(getv "$var")"
        if [ "$val" ]
            then setv "$var" "${val} ${@}"
            else setv "$var" "$@"
        fi
    fi
}

contain() {
    local v="$1"
    shift
    for i in "$@"
    do
        [ "$i" = "$v" ] && return 0
    done
    false
}

#### FORMATTING ####

fmt_tty() {
    while [ $# -gt 1 ]
    do
        local i
        for i in $(echo -n "$1" | sed -e 's/\(.\)/\1\n/g')
        do
            case "$i" in
                (h) tput bold    ;;
                (u) tput smul    ;;
                (r) tput setaf 1 ;;
                (g) tput setaf 2 ;;
                (y) tput setaf 3 ;;
                (b) tput setaf 4 ;;
            esac
        done
        shift
    done
    echo -n "$@"
    tput sgr0
}

fmt() {
    local str
    local arg
    local opt
    local echoopt
    local noopt=false
    while [ $# -gt 0 ]
    do
        arg="$1"
        if "$noopt"; then
            str="${str}${@}"
            break
        else
            case "$arg" in
                (--) noopt=true ;;
                (-n) echoopt='-n' ;;
                (-*) opt="$(echo -n "$arg" | sed -ne 's/^-\(.\+\)$/\1/p')" ;;
                (*)
                    if [ "$opt" ] && [ -t 1 ]; then
                        str="${str}$(fmt_tty "$opt" "$arg")"
                    else
                        str="${str}${arg}"
                    fi
                    opt=
                    ;;
            esac
        fi
        shift
    done
    echo $echoopt "$str"
}

debug_p() { [ "$LOG_LEVEL" -ge 4 ] && fmt >&2 "$@";                           }
debug()   { [ "$LOG_LEVEL" -ge 4 ] && fmt >&2 "${prog}: " -hg 'debug' ': ' "$@"; }
info_p()  { [ "$LOG_LEVEL" -ge 3 ] && fmt >&2 "$@";                           }
info()    { [ "$LOG_LEVEL" -ge 3 ] && fmt >&2 "${prog}: " -hg 'info' ': ' "$@";  }
warn_p()  { [ "$LOG_LEVEL" -ge 2 ] && fmt >&2 "$@";                           }
warn()    { [ "$LOG_LEVEL" -ge 2 ] && fmt >&2 "${prog}: " -hy 'warn' ': ' "$@";  }

error() {
    [ "$LOG_LEVEL" -ge 1 ] && fmt >&2 "${prog}: " -hr 'error' ': ' "$@"
    false
}

fatal() {
    [ "$LOG_LEVEL" -ge 1 ] && fmt >&2 "${prog}: " -hr 'fatal' ': ' "$@"
    exit 1
}

#### JQLIB ####

jqlib='
def is_obj_val($k; $t):
    type=="object" and has($k) and (.[$k] | type)==$t;

def is_class($class):
    is_obj_val("class"; "string") and .class==$class;

def is_block:
    type=="object" and (has("class") | not) and has("block")
    and (.block | type)=="array";

def is_fact:
    type=="object" and has("class") and .class=="fact";

def flatten_blocks:
    [ .[] | if is_block then .block | flatten_blocks[] else . end ];

def set_class_names:
    map(if (type=="object") and (has("class") | not)
        then keys_unsorted[0] as $class
            | .class=$class | .name=.[$class] | del(.[$class])
        else . end);

def to_final_hierarchy:
    def filter_facts:
        if is_fact then
            .[$facts[.name]] // empty | to_final_hierarchy[]
        else . end;
    def filter_targets:
        if is_class("targets") then
            if $targets | contains(["all"]) then
                to_entries[] | select(.key!="class") | .value
            else
                .[$targets[]] // empty
            end | to_final_hierarchy[]
        else . end;
    flatten_blocks | set_class_names | map(filter_facts | filter_targets);

def get_include_index:
    [ path(.. | if is_obj_val("include"; "string") then .include else empty end) ] as $paths
    | [[getpath($paths[])], [$paths[] | .[:length-1]]]
    | transpose | map({ file:.[0], path:.[1] });

def apply_includes($includes):
    reduce $includes[] as $i (.; setpath(
        $i.path;
        if $i.content | type=="object" then $i.content else { block: $i.content } end));

def get_include_file_paths:
    map(.file) | join("\n");

def get_names_from($class):
    [.[] | select(is_class($class)) | .name] | flatten | unique | join(" ");

def remove_comments:
    [ .[] | select(type!="string") ];

def merge_template_params:
    map(if is_class("template") and (.params | type=="array")
        then (.params=(reduce .params[] as $x ({}; . * $x)))
        else . end);

def merge_first($class):
    [
        { class: $class, name: ([.[] | select(is_class($class)) | .name ] | flatten | unique)},
        (.[] | select(is_class($class) | not))
    ];

def escape_filename:
    (split(" ") | join("\\ "));

def make_path_absolute:
    if .[0:1] == "/" then "" else "~/" end + .;

def escape_file_names:
    .name as $name |
    .from as $from |
    .esc_name |= ($name | escape_filename | make_path_absolute) |
    .sh_esc_from |= ($from | @sh) |
    .esc_from |= ($from | escape_filename) |
    .sh_esc_name |= (if $name[0:1] == "/" then "" else "~/" end + ($name | @sh)) |
    .sh_esc_dir |= (if $name[0:1] == "/" then "" else "~/" end + ($name | capture("^(?<dir>.*)/[^/]+/*$").dir | @sh)) ;

def make_link:
    if is_class("link")
    then @sh "make_link \(.location) \(.target) ;"
    else . end;

def rule_indices($class):
    map(.class) | indices($class) | join(" ");

def get_template_attr($rule_ind; $attr):
    .[$rule_ind][$attr];

def get_template_sed_exprs($rule_ind):
    [ (.[$rule_ind].params | to_entries[] | ("-e", ("s|{{ \(.key) }}|\(.value | tostring)|g" | @sh))) ] | join(" ");

def make_file($diff; $mkdir; $cp; $rm):
    "\n" +
    "\(.esc_name): \(.esc_from)\n" +
    "	@if ! \($diff) -q \(.sh_esc_from) \(.sh_esc_name) >/dev/null 2>&1; then \\\n" +
    "		echo '\''\($rm) -rf '\''\(.sh_esc_name) && \\\n" +
    "		\($rm) -rf \(.sh_esc_name) && \\\n" +
    "		echo '\''\($mkdir) -p '\''\(.sh_esc_dir) && \\\n" +
    "		\($mkdir) -p \(.sh_esc_dir) && \\\n" +
    "		echo '\''\($cp) -R '\''\(.sh_esc_from) \(.sh_esc_name) && \\\n" +
    "		\($cp) -R \(.sh_esc_from) \(.sh_esc_name) ; \\\n" +
    "	fi\n" +
    "status-\(.esc_name):\n" +
    "	@if \($diff) -q \(.sh_esc_from) \(.sh_esc_name) >/dev/null 2>&1; then \\\n" +
    "		echo '\''[X] file: '\''\(.sh_esc_name) ; \\\n" +
    "	else \\\n" +
    "		echo '\''[ ] file: '\''\(.sh_esc_name) ; \\\n" +
    "	fi";

def make_files($diff; $mkdir; $cp; $rm):
    map(select(is_class("file")) | escape_file_names) | (
        (.[] | make_file($diff;$mkdir;$cp;$rm)),
        (
            "\nfiles: \(map(.esc_name) | join(" "))\n" +
            "status-files: " + (map("status-" + .esc_name) | join(" "))
        )
    );
'

jq_json() { "$bin_jq" -cj "$@"; }
jq_raw() { "$bin_jq" -r "$@"; }

jq_v() {
    local v="$1"
    shift
    echo -n "$v" | jq_json "$@"
}

jq_raw_v() {
    local v="$1"
    shift
    echo -n "$v" | jq_raw "$@"
}

jq_update() {
    local var="$1"
    shift
    setv "$var" "$(getv "$var" | jq_json "$@")"
}

#### MAKE ####

make_start() {
    if [ "$facts_distro" = arch ]; then
        local pkgs_extra='status-aurs'
    fi
    cat <<EOF
#
# Makefile generated by pas2tir
# Date: $(date --rfc-3339=seconds)
#
# For:
#   Distribution: ${facts_distro}
#   Architecture: ${facts_arch}
#   Graphics:     ${facts_graphics}
#   Hostname:     ${facts_hostname}
#

all:

status: status-pkgs ${pkgs_extra} status-files status-templates
EOF
}

#1: class
#@: packages
make_packages() {
    local class="$1"
    local class_caps="$(echo -n "$1" | tr '[:lower:]' '[:upper:]')"
    shift
    case "$facts_distro" in
        (arch)
            case "$class" in
                (pkg)
                    local pkgtest='pacman -Qq $* || pacman -Qqg $*'
                    local pkginstall='pacman -S --needed'
                    ;;
                (aur)
                    local pkgtest='pacman -Qq $* || pacman -Qqg $*'
                    local pkginstall='yay -S --needed'
                    ;;
            esac
            ;;
        (fedora)
            case "$class" in
                (pkg)
                    local pkgtest='rpm -V --nodeps --nodigest --nofiles --noscripts --nosignature --nolinkto --nofiledigest --nosize --nouser --nogroup --nomtime --nomode --nordev --nocaps $*'
                    local pkginstall='yum install -y'
                    ;;
            esac
            ;;
        (*)
            local pkgtest='false'
            local pkginstall='false'
            ;;
    esac
    cat <<EOF

${class_caps}S = ${@}
${class_caps}_RULES = \$(${class_caps}S:%=${class}-%)

install-${class}s:
	${pkginstall} \$(${class_caps}S)
status-${class}s: \$(${class_caps}_RULES)
\$(${class_caps}_RULES):${class}-%:
	@if { ${pkgtest}; } >/dev/null 2>&1; then echo '[X] package: \$*'; else echo '[ ] package: \$*'; fi
EOF
}

#1: file
#2: location
#3: sed expressions
make_template_rule() {
    local cache="cache/${2}"
    cat <<EOF

.PHONY: ${cache}
${cache}: ${1}
	@${bin_mkdir} -p $(dirname "$cache") && ${bin_sed} ${3} \$^ > \$@
EOF
}

#1: config
make_templates() {
    local cfg="$1"
    local list
    local i
    echo
    for i in $(jq_raw_v "$cfg" "$jqlib"'rule_indices("template")')
    do
        debug "process rule nº${i} (template)"
        sed_exprs="$(jq_raw_v "$cfg" --arg i "$i" "$jqlib"'get_template_sed_exprs($i | tonumber)')"
        location="$(jq_raw_v "$cfg" --arg i "$i" "$jqlib"'get_template_attr($i | tonumber; "name") | make_path_absolute' \
            | sed 's/^ *~/$(HOME)/' | eval "${bin_sed} ${sed_exprs}")"
        file="$(jq_raw_v "$cfg" --arg i "$i" "$jqlib"'get_template_attr($i | tonumber; "from")' \
            | eval "${bin_sed} ${sed_exprs}")"
        make_template_rule "$file" "$location" "$sed_exprs"
        push list "$location"
    done
    cat <<EOF

TMPLS = ${list}
STATUS_TMPLS = \$(TMPLS:%=status-%)
DIFF_TMPLS = \$(TMPLS:%=diff-%)

\$(TMPLS): %: cache/%
	@if ! ${bin_diff} -q \$^ \$@ >/dev/null 2>&1; then \\
		echo '${bin_mkdir} -p \$(dir \$@) && ${bin_cp} \$^ \$@'; \\
		${bin_mkdir} -p \$(dir \$@) && ${bin_cp} \$^ \$@; \\
	fi
\$(STATUS_TMPLS): status-%: cache/%
	@if ${bin_diff} -q \$^ \$* >/dev/null 2>/dev/null; then \\
		echo '[X] template: \$*'; \\
    else \\
		echo '[ ] template: \$*'; \\
    fi
\$(DIFF_TMPLS): diff-%: cache/%
	@${bin_diff} --color=auto \$* \$^ ; true

templates: \$(TMPLS)
status-templates: \$(STATUS_TMPLS)
EOF
}

#1: config
make_files() {
    debug 'generate file rules'
    jq_raw_v "$1" --arg cp "$bin_cp" --arg diff "$bin_diff" --arg mkdir "$bin_mkdir" --arg rm "$bin_rm" \
        "$jqlib"'make_files($diff; $mkdir; $cp; $rm)'
}

#### FACTS ####

get_graphics_fact() {
    "$bin_lspci" | "$bin_grep" -Eq ' 3D .+ NVIDIA ' \
        && "$bin_lspci" | "$bin_grep" -Eq ' VGA .+ Intel ' \
        && { info 'GPU is ' -hy 'NVIDIA Optimus'; echo -n 'optimus'; return; }
    "$bin_lspci" | "$bin_grep" -Eq ' VGA .+ Radeon ' \
        && { info 'GPU is ' -hy 'ATI'; echo -n 'ati'; return; }
    debug 'GPU is ' -hy 'unknown'
    echo -n 'unknown'
}

get_distro_fact() {
    local distro="$("$bin_lsb_release" -is | tr '[:upper:]' '[:lower:]')"
    info 'Linux distribution is ' -h "$distro"
    echo -n "$distro"
}

get_arch_fact() {
    local arch="$("$bin_uname" -m)"
    info 'architecture is ' -h "$arch"
    echo -n "$arch"
}

get_hostname_fact() {
    local hostname="$("$bin_hostname")"
    info 'hostname is ' -h "$hostname"
    echo -n "$hostname"
}

set_facts_env() {
    facts_distro="$(get_distro_fact)"
    facts_arch="$(get_arch_fact)"
    facts_graphics="$(get_graphics_fact)"
    facts_hostname="$(get_hostname_fact)"
}

get_json_facts()
{
    "$bin_jq" -cjn \
        --arg distro "$facts_distro" \
        --arg arch "$facts_arch" \
        --arg graphics "$facts_graphics" \
        --arg hostname "$facts_hostname" \
        '
        {
            distro: $distro,
            arch: $arch,
            graphics: $graphics,
            hostname: $hostname
        }
        '
}

#### DEPENDENCIES ####

dependencies='
cp
diff
grep
hostname
jq
ln
lsb_release
lspci
make
mkdir
rm
sed
tput
uname
'

set_dependencies() {
    local bin
    local ok
    for i in $@
    do
        bin="$(command -v "$i")"
        ok="$?"
        setv "bin_${i}" "$bin"
        if [ "$ok" -eq 0 ]; then
            debug -h "$i"  ' is ' -hg 'installed'
            setv "dep_${i}" true
        else
            debug -h "$i"  ' is ' -hr 'not installed'
            setv "dep_${i}" false
        fi
    done
}

process_includes() {
    local cfg="$(cat)"

    local incl_it_c=1
    local incl_idx
    local includes
    while {
        debug "include lookup iteration nº${incl_it_c}"
        incl_idx="$(jq_v "$cfg" "$jqlib"get_include_index)"
        incl_it_c=$((incl_it_c + 1))
        [ "$incl_idx" ] && [ "$incl_idx" != '[]' ]
    }
    do
        includes="$incl_idx"
        local c=0
        for i in $(jq_raw_v "$incl_idx" "$jqlib"get_include_file_paths)
        do
            debug 'load file ' -h "$i"
            jq_update includes --argjson c "$c" --slurpfile content "$i" \
                'setpath([$c, "content"]; $content[])' \
                || fatal "failed to import JSON file: " -h "$i"
            c=$(($c + 1))
        done

        debug 'insert included files'
        jq_update cfg --argjson includes "$includes" "$jqlib"'apply_includes($includes)'
    done

    debug 'all files included'
    echo -n "$cfg"
}

#1: facts
#2: targets
prepare_config() {
    process_includes | {
        debug 'flatten blocks hierarchy'
        debug 'filter configuration tree according to facts'
        debug 'filter configuration tree according to targets'
        debug 'remove class syntactic sugar'
        debug 'prepare template parameter lists'
        debug 'group "pkg" and "user" rules'
        jq_json --argjson facts "$1" --argjson targets "$2" "$jqlib"'
            to_final_hierarchy
            | remove_comments
            | merge_template_params
            | merge_first("user")
            | merge_first("aur")
            | merge_first("pkg")'
    }
}

main() {
    local i

    set_dependencies $dependencies
    set_facts_env
    facts="$(get_json_facts)"

    local targets="$(jq_json -n --args '$ARGS.positional' "$@")"
    local cfg="$(prepare_config "$facts" "$targets")"

    [ "$DEBUGFILE" ] && echo -n "$cfg" | "$bin_jq" . > "$DEBUGFILE"

    make_start

    make_packages pkg $(jq_raw_v "$cfg" "$jqlib"'get_names_from("pkg")')
    if [ "$facts_distro" = arch ]; then
        make_packages aur $(jq_raw_v "$cfg" "$jqlib"'get_names_from("aur")')
    fi

    make_templates "$cfg"
    make_files "$cfg"

}

main "$@"
